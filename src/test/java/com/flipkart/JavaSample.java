package com.flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.List;

public class JavaSample {

    public static final String huburl = "http://192.168.0.100:5566/wd/hub";
    static WebDriverWait wait;
    static JavascriptExecutor jse;
    static String searchItem = "iphone 6";

    @Test
    public static void main() throws Exception {

        // UnComment code to if run on Selenium Server

        /*DesiredCapabilities dc =new  DesiredCapabilities();
        dc.setBrowserName("chrome");
        dc.setPlatform(Platform.MAC);
        driver = new RemoteWebDriver(new URL("huburl"), dc);*/

        // Below is code to run in Local driver
        WebDriver browser = new ChromeDriver();
        test_case(browser);
        browser.quit();
    }


    public static void test_case(WebDriver driver) {

        driver.manage().window().fullscreen();
        driver.get("https://www.flipkart.com/");
        wait = new WebDriverWait(driver, 30);
        jse = (JavascriptExecutor) driver;

        //Clicked on login cross Button
        driver.findElement(By.xpath("//button[@class='_2AkmmA _29YdH8']")).click();
        WebElement element = driver.findElement(By.cssSelector("input[title='Search for products, brands and more']"));
        element.sendKeys(searchItem);

        // Wait till result appear iphone 6
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[@class='_1va75j']//span[text()='iphone 6']")));

        // Click on Iphone 6 mobile category
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li[@class='_1va75j']//div[text()='in Mobiles'])[2]")));
        jse.executeScript("arguments[0].click();", element1);


        // Selecting price 30K +
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//select[@class='fPjUPw'])[1]"))).click();
        Select min = new Select(driver.findElement(By.xpath("(//select[@class='fPjUPw'])[1]")));
        min.selectByIndex(9);


        // wait for filter to updated
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_3clSXm']")));


        // wait for filter to updated to Flipkart Assured
        driver.findElement(By.xpath("//label//input[@type='checkbox']//following::div[@class='_1p7h2j _2irnD_']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_3clSXm']//div[text()='Flipkart Assured']")));


        //click and  wait for filter to updated to Brand Apple
        WebElement apple = driver.findElement(By.xpath("//label//input[@type='checkbox']//following::div[text()='Apple']"));
        jse.executeScript("arguments[0].click();", apple);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_3clSXm']//div[text()='Apple']")));

        // Xpath for Iphone name

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_3wU53n']")));
        List<WebElement> iphoneName = driver.findElements(By.xpath("//div[@class='_3wU53n']"));
        for (WebElement we : iphoneName) {
            System.out.println("Iphone Name --->" + we.getText());
        }


        // Xpath for Iphone Price
        List<WebElement> iphonePrice = driver.findElements(By.xpath("//div[@class='_1vC4OE _2rQ-NK']"));
        for (WebElement we : iphonePrice) {
            System.out.println("Iphone Price--->" + we.getText());
        }


        // Xpath for href  Link of product
        List<WebElement> iphoneLink = driver.findElements(By.xpath("//a[@class='_31qSD5']"));
        for (WebElement we : iphoneLink) {
            System.out.println("Iphone Hyperlink---->" + we.getAttribute("href"));
        }

    }

}


